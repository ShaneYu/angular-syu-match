angular-syu-match
=====================

A validation directive that ensures that one field matches another. Useful for confirming passwords, emails or anything.

The "ng-match" attribute should be set equal to the ng-model value of the field to match.

'match', 'data-match' and 'ng-match' are all valid attributes that can be used for this directive, see the usage examples below for more help.

Installation
------------

    $ bower install angular-syu-match


Usages
-------

  ** Example using match attribute**
  ```html
  Password: <input type="password" ng-model="user.password" />
  Confirm: <input type="password" ng-model="user.passwordConfirm" match="user.password" />
  ```
  
  ** Example using data-match attribute**
  ```html
  Password: <input type="password" ng-model="user.password" />
  Confirm: <input type="password" ng-model="user.passwordConfirm" data-match="user.password" />
  ```
  
  ** Example using ng-match attribute**
  ```html
  Password: <input type="password" ng-model="user.password" />
  Confirm: <input type="password" ng-model="user.passwordConfirm" ng-match="user.password" />
  ```
  
  ** Example using ng-match attribute with custom error message**
  If your form and field both are named, you can access the validation result to show/hide messages
  ```html
  <form name="myForm">
  Password: <input type="password" ng-model="user.password" />
  Confirm: <input type="password" ng-model="user.passwordConfirm" ng-match="user.password" name="txtPasswordConfirm" id="txtPasswordConfirm" />
  <div ng-show="myForm.txtPasswordConfirm.$error.match">Fields do not match!</div>
  </form>
  ```

  
License
-------

Licensed under the terms of the MIT License.