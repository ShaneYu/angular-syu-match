/**
 * Created by yus on 29/08/2014.
 *
 * Match Validation Directive
 *
 * A validation directive that ensures that one field matches another. Useful for confirming passwords, emails or anything.
 * The "ng-match" attribute should be set equal to the ng-model value of the field to match.
 */

'use strict';

var matchDirective = function () {
    return {
        restrict: 'A',
        require: 'ngModel',
        scope: {
            match: '=',
            ngMatch: '='
        },
        link: function (scope, elm, attr, ctrl) {
            if (!ctrl) return;
            scope.$watch(function() {
                var modelValue = ctrl.$modelValue || ctrl.$$invalidModelValue;
                return (ctrl.$pristine && angular.isUndefined(modelValue)) || (scope.match || scope.ngMatch) === modelValue;
            }, function (currentValue) {
                ctrl.$setValidity('match', currentValue);
            });
        }
    };
};

angular.module('syu.directives').directive('match', matchDirective);
angular.module('syu.directives').directive('ngMatch', matchDirective);